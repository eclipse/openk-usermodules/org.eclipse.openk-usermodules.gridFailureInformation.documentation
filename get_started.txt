 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GET STARTED
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

**********************************************************************
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! *
PLEASE BE SURE THAT YOU HAVE FIRST PORTAL INSTALLED AND CONFIGURED ! *
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! *
**********************************************************************

Here you find a step-by-step instruction on how to build all documentation in this repository including:
- architectureDocumentation.adoc
- howtoBuild.adoc
- howtoConfigure.adoc
- howtoRun.adoc
- userDocumentation.adoc
At the end you have a pdf and a html version of each adoc (documentation).

---------------- REQUIREMENTS -------------
* Java 1.8
* Apache Maven 3.6.1
* Graphviz

If you meet this requirements, check if you have set JAVA_HOME and M2_HOME.
    - If yes: Ignore step 1,2 and 3
    - If no: Follow the corresponding steps below


-------------------- 1 ---------------------
Install java 8

- Open a cmd and check your java version
    $ java -version
- If you dont have java version 1.8 then download and install it from:
        http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- Set JAVA_HOME (Windows OS):
    Right click on This Computer and select Properties.
    On the Advanced tab, select Environment Variables, and then edit JAVA_HOME to point to where the JDK software is located, for example, C:\Program Files\Java\jdk1.8.xxx.
    Also, edit The Path variable in System variables and add the bin path (C:\Program Files\Java\jdk1.8.xxx\bin)
- Restart your PC
- Confirm by checking the version
    $ java -version


-------------------- 2 ---------------------
Install Apache Maven to your pc

Maven is a tool that can be used for building and managing any Java-based project.

- Download maven 3.6.1 from
        https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.6.1/
- Extract the folder and place it on your C drive
- Set M2_HOME (Windows OS):
    - Right click on This Computer and select Properties.
    - On the Advanced tab, select Environment Variables, and then create M2_HOME to point to where the Maven software is located, for example, C:\apache-maven-3.5.0.
    - Also edit The Path variable in System variables and add the bin path (C:\apache-maven-3.5.0\bin)
- Restart your PC
- Confirm with
    $ mvn -v

-------------------- 3 ---------------------
Install Graphviz
Graphviz is open source graph visualization software. You need this software because of auto-generated graphics in the documentations.

* Download and install Graphviz from https://www.graphviz.org/download/
* If not already done, set the PATH variable

 Value of variable: <GRAPHVIZ_DIR>\bin

-------------------- 4 ---------------------
Building the documentation:
- Open a command line and navigate to the root folder of this project (same location of this very get_started.txt file)
- Use the following call to cleanly build and install artifacts:
    $ mvn clean install

-------------------- 5 ---------------------
If you successfully build the maven project you can find in /target/generated-docs all the documentations of this projects
in pdf and html form, choose according to your liking:
    - "howtoBuild" -> here you can read how to build the whole (backend and frontend) project
    - "howtoConfigure" -> here you can read how to configure the Application
    - "howtoRun" -> here you can read how to run/start the Application
    - "architectureDocumentation" -> here you can read about the architecture pf this module


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
NEXT STEP -> READ THE howtoBuild FILE             !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!